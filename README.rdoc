== README

This is a recipe management app
Things you may want to cover:

*Migration to create reviews table , body, type = text_field , two forgien keys chef_id and recipe_id
*Build the review model
*youll need to create tests
*build the association , has_many and belongs_to
*recipe.reviews
*chef.reviews
*recipe with review, show page for recipe
resources reviews, scope
authenticated chefs can create review, using before_action
associate chef_id and recipe_id
etc

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...


Please feel free to use a different markup language if you do not plan to run
<tt>rake doc:app</tt>.
