require 'test_helper'

class ChefTest < ActiveSupport::TestCase

  def setup
    @chef = Chef.new(chefname: "John", email: "john@example.com")
  end

  test "chef should be valid" do
      assert @chef.valid?
  end

  test "chef name should be present" do
    @chef.chefname = ""
    assert_not @chef.valid?
  end

  test "chef name should not be too long" do
    @chef.chefname = "a" * 41
    assert_not @chef.valid?
  end

  test "chef name should not be too short" do
    @chef.chefname = "aa"
    assert_not @chef.valid?
  end

  test "email should be present" do
    @chef.email = ""
    assert_not @chef.valid?
  end

  test "email length should be limited" do
    @chef.email = "a" * 100 + "example.com"
    assert_not @chef.valid?
  end

  test "email address should be unique" do
    dup_chef = @chef.dup
    dup_chef.email = @chef.email.upcase
    @chef.save
    assert_not dup_chef.valid?
  end

  test "email address should be valid" do
    valid_addresses = %w[user@ee.com R_TDD-S@see.hello.org user@example.com first.last@email.com jack+sparrow@jw.com]
    valid_addresses.each do |e|
      @chef.email = e
      assert @chef.valid?, '#{e.inspect} should be valid'
    end
  end

  test "email validation should reject invalid addresses" do
    invalid_addresses = %w[user@example,com user-att_org.org user.name@example. _iamfoo@+ier.com]
    invalid_addresses.each do |f|
      @chef.email = f
      assert_not @chef.valid?, '#{f.inspect} should be invalid'
    end
  end




end
