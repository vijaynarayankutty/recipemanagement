class CommentsController < ApplicationController
  before_action :require_user, except: [:show, :index]

  def index
    # @comments = Comment.all
  end

  def show
  end

  def new
    @comment = Comment.new
  end

  def create
    @comment = Comment.new(comment_params)
    #@comment.recipe = Recipe.find(13) #temporary implementation
    @comment.recipe = Recipe.find(params[:id])
    @comment.chef = current_user
    if @comment.save
      flash[:success] = "You posted a comment"
      redirect_to recipe_path
    else
      render 'new'
    end
  end

  private

  def comment_params
    params.require(:comment).permit(:comment, :recipe_id)
  end

end
