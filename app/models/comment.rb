class Comment < ActiveRecord::Base
  belongs_to :chef
  belongs_to :recipe
  validates :comment, presence: true, length: {minimum: 2}

  def comment_total
    comment = Comment.all
    comment.size
  end

end
